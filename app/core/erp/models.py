from datetime import datetime

from django.db import models

class type(models.Model):
    names = models.CharField(max_length = 150, verbose_name = 'Nombres')

class Meta:
    verbose_Name = 'Tipo'
    verbose_Name_plural = 'Tipos'
    ordering = ['Id']


class Employee(models.Model):
    type = models.ForeignKey(type, on_delete = models.CASCADE)
    names = models.CharField(max_length=150, verbose_name='Nombres')
    cui = models.CharField(max_length=20, verbose_name= 'cui')
    date = models.DateField(default=datetime.now, verbose_name='Fecha de ingreso')
    age = models.PositiveIntegerField(default=0)
    salary = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)

class Meta:
    verbose_Name = 'Empleado'
    verbose_Name_plural = 'Empleados'
    db_table = 'empleado'
    ordering = ['Id']
